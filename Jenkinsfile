node('Jenkins') {

  def apply = false

  stage('SETUP') {

    echo "PARAMETROS: "
    echo "Ambiente: ${params.ambiente}"
    echo "Project name: ${params.nombre_proyecto}"
    echo "Git proyecto terraform: ${params.url_git_proyecto_terraform}"
    echo "Git configuracion terraform: ${params.url_git_configuracion_terraform}"

    deleteDir()

    // Se carga en la variable path la aplicacion terraform.
    def tfHome = tool name: 'terraform', type: 'com.cloudbees.jenkins.plugins.customtools.CustomTool'
    env.PATH = "${tfHome}:${env.PATH}"

    // Se obtiene la credencial de GCP del credential store de jenkins y se añade al archivo para su uso.
    sh 'mkdir -p creds'
    withCredentials([string(credentialsId: 'terraform-auth', variable: 'SVC_ACCOUNT_KEY')]) {
      sh 'echo $SVC_ACCOUNT_KEY | base64 -d > ./creds/osde-terraform-auth.json'
    }    
  }

  stage('CHECKOUT') {

    sh 'pwd'
    sh 'ls -la'

    // Se hace el checkout de la configuracion de terraform desde la rama 'master'. El resultado se coloca en la carpeta 'conf'.
    checkout([$class: 'GitSCM', branches: [[name: '*/master']], doGenerateSubmoduleConfigurations: false, extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: './conf']], submoduleCfg: [], userRemoteConfigs: [[credentialsId: 'gitlab', url: "${params.url_git_configuracion_terraform}"]]])
    sh 'ls -la ./conf'

    // Se hace el checkout del TFSTATE de configuracion de terraform desde la rama 'ambiente/project/state'. El resultado se coloca en la carpeta 'state'.
    checkout([$class: 'GitSCM', branches: [[name: "*/${params.ambiente}/${params.nombre_proyecto}/state"]], doGenerateSubmoduleConfigurations: false, extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: './state']], submoduleCfg: [], userRemoteConfigs: [[credentialsId: 'gitlab', url: "${params.url_git_configuracion_terraform}"]]])
    sh 'ls -la ./state'

    // Se hace el checkout de la infraestructura de terraform desde la rama 'ambiente'. El resultado se coloca en la carpeta 'tf'.
    checkout([$class: 'GitSCM', branches: [[name: "*/${params.ambiente}"]], doGenerateSubmoduleConfigurations: false, extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: './tf']], submoduleCfg: [], userRemoteConfigs: [[credentialsId: 'gitlab', url: "${params.url_git_proyecto_terraform}"]]])
    sh 'ls -la ./tf'

    // Se juntan todos los archivos en el working directory.
    sh 'mv ./conf/* ./ || true'
    sh 'mv ./state/* ./ || true'
    sh 'mv ./tf/* ./ || true'

    // Se eliminan las carpetas sin uso.
    sh 'rm -rf ./conf conf@tmp state@tmp ./tf tf@tmp'
    sh 'ls -la'
  
  }

  stage('TERRAFORM INIT') {
    sh "terraform -version"
    sh 'terraform init -input=false'
  }

  stage('TERRAFORM VALIDATE') {
    def project_id = "${params.nombre_proyecto}"
    sh "terraform validate -var project-id=$project_id -check-variables=true"
  }

  stage('TERRAFORM PLAN') {
    def successNoChanges = "0"
    def error = "1"
    def successChangesPresent = "2"
    def project_id = "${params.nombre_proyecto}"
    sh "set +e; terraform plan -var project-id=$project_id -input=false -out=plan.out -detailed-exitcode; echo \$? > status.plan"

    def planExitCode = readFile('status.plan').trim()
    if (fileExists("status.plan")) {
        sh "rm status.plan"
    }
    echo "Terraform plan exit code: ${planExitCode}"

    if (planExitCode == successNoChanges) {
      currentBuild.result = 'SUCCESS'
    } else if (planExitCode == error) {
      currentBuild.result = 'FAILURE'
    } else if (planExitCode == successChangesPresent) {
      input message: 'Apply Plan?', ok: 'Apply'
      apply = true
    }
  }

  stage('TERRAFORM APPLY') {
    sh 'ls -l'
    sh 'pwd'
    if (apply) {

      def success = "0"
      sh 'set +e; terraform apply -input=false plan.out; echo $? > status.apply'

      def applyExitCode = readFile('status.apply').trim()
      if (fileExists("status.apply")) {
        sh "rm status.apply"
      }
      echo "Terraform apply exit code: ${applyExitCode}"

      if (applyExitCode == success) {
        currentBuild.result = 'SUCCESS'
      } else {
        currentBuild.result = 'FAILURE'
      }
    }
  }

  stage('UPDATE REMOTE TFSTATE') {
    if (apply && fileExists("terraform.tfstate")) {

      // Se mueven los archivos TFSTATE a la carpeta 'state' para actualizar el repositorio.
      sh 'mv ./terraform.tfstate ./state'
      sh 'mv ./terraform.tfstate.backup ./state || true'

      // Nos paramos en la carpeta 'state' para subir el TFSTATE al repositorio.
      dir('./state') {
        withCredentials([usernamePassword(credentialsId: 'gitlab', passwordVariable: 'GIT_PASSWORD', usernameVariable: 'GIT_USERNAME')]) {
          sh 'ls -la'

          sh 'git config --global user.name $GIT_USERNAME'
          sh 'git config --global user.email $GIT_USERNAME'

          sh 'git add ./terraform.tfstate' 
          if (fileExists("terraform.tfstate.backup")) {
              sh 'git add ./terraform.tfstate.backup' 
          }

          sh 'git commit -m "jenkins update tfstate: BUILD_TAG - $BUILD_TAG / BUILD_URL - $BUILD_URL"'

          // Se carga en una variable el branch y el repositorio de configuracion donde se subira el TFSTATE.
          def branch = "${params.ambiente}/${params.nombre_proyecto}/state"
          def (prefix, repositorio) = "${params.url_git_configuracion_terraform}".split( '//' )

          // git push https://username:password@git.osde.ar/project.git HEAD:branch_name
          sh "git push $prefix//$GIT_USERNAME:$GIT_PASSWORD@$repositorio HEAD:$branch"
        }
      }
    }
  }

}
