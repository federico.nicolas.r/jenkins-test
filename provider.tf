provider "google" {
    project     = "${var.project-id}"
    credentials = "./creds/osde-terraform-auth.json"
}
